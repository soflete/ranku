package com.example.names

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.androideas.core.data.Item
import com.androideas.core.data.Type
import com.androideas.core.extension.textChanges
import com.example.names.databinding.FragmentNamesBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NamesFragment : Fragment() {

    private val viewModel: NamesViewModel by viewModels()

    private lateinit var binding: FragmentNamesBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        this.binding = FragmentNamesBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {

            genderRadioGroup.setOnCheckedChangeListener { _, checkedId ->
                viewModel.onGenderChanged(
                    when (checkedId) {
                        R.id.femaleRadioButton -> Gender.FEMALE
                        R.id.maleRadioButton -> Gender.MALE
                        else -> Gender.ANY
                    }
                )
            }

            modeRadioGroup.setOnCheckedChangeListener { _, checkedId ->
                viewModel.onModeChanged(
                    when (checkedId) {
                        R.id.containsRadioButton -> Mode.CONTAINS
                        R.id.endsRadioButton -> Mode.ENDS
                        else -> Mode.STARTS
                    }
                )
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    binding.txtInput.textChanges()
                        .debounce(386)
                        .filterNotNull()
                        .map { it.toString() }
                        .collect(viewModel::onLoadNames)
                }
            }

            results.setOnItemClickListener { v, _, position, _ ->
                v.getItemAtPosition(position)
                    .let { it as String }
                    .let { Item(0, it, Type.NAME, it) }
                    .let { item -> Intent().putExtra(Item::class.simpleName, item as Parcelable) }
                    .let { intent ->
                        activity?.apply {
                            setResult(android.app.Activity.RESULT_OK, intent)
                            finish()
                        }
                    }
            }
        }

        viewModel.liveState.observe(viewLifecycleOwner) { (_, gender, mode, names) ->

            this.binding.genderRadioGroup.check(
                when (gender) {
                    Gender.FEMALE -> R.id.femaleRadioButton
                    Gender.MALE -> R.id.maleRadioButton
                    Gender.ANY -> R.id.allRadioButton
                }
            )

            this.binding.modeRadioGroup.check(
                when (mode) {
                    Mode.STARTS -> R.id.startsRadioButton
                    Mode.CONTAINS -> R.id.containsRadioButton
                    Mode.ENDS -> R.id.endsRadioButton
                }
            )

            names?.let { ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, it) }
                ?.let { this.binding.results.adapter = it }
        }
    }
}
