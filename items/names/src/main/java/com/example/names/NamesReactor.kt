package com.example.names

import javax.inject.Inject

class NamesReactor @Inject constructor(
        private val getNamesUseCase: GetNamesUseCase,
        private val namesPreferences: NamesPreferences
) {

    suspend fun react(event: NamesEvent, state: NamesState): NamesEvent? = when (event) {
        is NamesEvent.ChangeQuery -> NamesEvent.LoadNames
        is NamesEvent.ChangeGender -> {
            namesPreferences.gender = event.gender
            NamesEvent.LoadNames
        }
        is NamesEvent.ChangeMode -> {
            namesPreferences.mode = event.mode
            NamesEvent.LoadNames
        }
        is NamesEvent.LoadNames -> state.let { (query, gender, mode, _) ->
            when {
                query != null && query.isNotBlank() -> getNamesUseCase.execute(query, gender, mode)
                        .map { it.name }
                        .let { NamesEvent.NamesLoaded(it) }
                else -> null
            }
        }
        else -> null
    }
}
