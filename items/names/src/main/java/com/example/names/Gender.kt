package com.example.names

enum class Gender { ANY, MALE, FEMALE }