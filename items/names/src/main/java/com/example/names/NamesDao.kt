package com.example.names

import androidx.room.Dao
import androidx.room.Query

@Dao
interface NamesDao {

    @Query(value = """
        SELECT * FROM names
        WHERE name LIKE :input
        AND gender LIKE :gender
        ORDER BY name ASC
    """)
    suspend fun search(input: String, gender: String): List<NamesRecord>
}
