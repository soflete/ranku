package com.example.names

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "names", primaryKeys = ["name", "gender"])
data class NamesRecord(
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "gender") val gender: Char
)
