package com.example.names

sealed class NamesEvent {
    data class ChangeGender(val gender: Gender) : NamesEvent()
    data class ChangeMode(val mode: Mode) : NamesEvent()
    data class ChangeQuery(val query: String) : NamesEvent()
    object LoadNames : NamesEvent()
    data class NamesLoaded(val names: List<String>) : NamesEvent()
}
