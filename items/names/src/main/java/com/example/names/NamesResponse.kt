package com.example.names

import com.squareup.moshi.Json

data class NamesResponse(
        val help: String, // Can be parsed as URL
        val success: Boolean,
        val result: Result?,
        val error: Error?,
) {

    data class Result(val records: List<Record>)
    data class Record(val Name: String)
    data class Error(@Json(name = "__type") val type: String)
}
