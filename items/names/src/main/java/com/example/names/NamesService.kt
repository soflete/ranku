package com.example.names

import retrofit2.http.GET
import retrofit2.http.Query

interface NamesService {

    @GET("/api/3/action/datastore_search_sql")
    suspend fun searchName(@Query("sql") sql: String): NamesResponse
}
