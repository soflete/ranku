package com.example.names

fun reduce(state: NamesState, event: NamesEvent) = when (event) {
    is NamesEvent.ChangeGender -> state.copy(gender = event.gender)
    is NamesEvent.ChangeMode -> state.copy(mode = event.mode)
    is NamesEvent.ChangeQuery -> state.copy(query = event.query)
    is NamesEvent.NamesLoaded -> state.copy(names = event.names)
    else -> state
}
