package com.example.names

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [NamesRecord::class], version = 1)
abstract class NamesDatabase : RoomDatabase() {
    abstract fun namesDao(): NamesDao
}
