package com.example.names

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.androideas.core.extension.reactIn
import com.androideas.core.extension.reduceIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NamesViewModel @Inject constructor(
        state: MutableStateFlow<NamesState>,
        private val reactor: NamesReactor
) : ViewModel() {

    private val events = MutableSharedFlow<NamesEvent>()

    val liveState = state.asLiveData()

    init {
        events.reactIn(viewModelScope, state, reactor::react)
        state.reduceIn(viewModelScope, events, ::reduce)
    }

    fun onLoadNames(name: String) {
        viewModelScope.launch { events.emit(NamesEvent.ChangeQuery(name)) }
    }

    fun onGenderChanged(gender: Gender) {
        viewModelScope.launch { events.emit(NamesEvent.ChangeGender(gender)) }
    }

    fun onModeChanged(mode: Mode) {
        viewModelScope.launch { events.emit(NamesEvent.ChangeMode(mode)) }
    }
}
