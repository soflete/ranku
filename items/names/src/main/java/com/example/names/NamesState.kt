package com.example.names

data class NamesState(
        val query: String? = null,
        val gender: Gender = Gender.ANY,
        val mode: Mode = Mode.STARTS,
        val names: List<CharSequence>? = null
)
