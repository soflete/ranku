package com.example.names

enum class Mode { STARTS, CONTAINS, ENDS }