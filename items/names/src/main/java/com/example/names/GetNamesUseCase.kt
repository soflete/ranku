package com.example.names

import javax.inject.Inject

class GetNamesUseCase @Inject constructor(private val dao: NamesDao) {
    suspend fun execute(query: String, gender: Gender, mode: Mode): List<NamesRecord> {
        return dao.search(when (mode) {
            Mode.STARTS -> "$query%"
            Mode.CONTAINS -> "%$query%"
            Mode.ENDS -> "%$query"
        }, when (gender) {
            Gender.ANY -> '%'
            Gender.MALE -> 'M'
            Gender.FEMALE -> 'F'
        }.toString())
    }
}
