package com.example.names

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.MutableStateFlow
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(ViewModelComponent::class)
object NamesModule {

    private const val BASE_URL = "https://catalogue.data.govt.nz"

    private val okHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build()
    }

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    @Provides
    fun namesPreferences(@ApplicationContext context: Context): NamesPreferences {
        return NamesPreferences(PreferenceManager.getDefaultSharedPreferences(context))
    }

    @Provides
    fun initialNamesState(namesPreferences: NamesPreferences) =
        NamesState(gender = namesPreferences.gender, mode = namesPreferences.mode)

    @Provides
    @ViewModelScoped
    fun namesStateFlow(initialNamesState: NamesState) = MutableStateFlow(initialNamesState)

    @Provides
    @ViewModelScoped
    fun database(@ApplicationContext context: Context) = Room
        .databaseBuilder(context, NamesDatabase::class.java, "Names.db")
        .createFromAsset("Names.db")
        .build()

    @Provides
    fun dao(database: NamesDatabase) = database.namesDao()

    @Provides
    fun namesService(): NamesService = retrofit.create(NamesService::class.java)
}
