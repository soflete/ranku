package com.example.names

import android.content.SharedPreferences
import com.androideas.core.extension.enumPreference

class NamesPreferences(sharedPreferences: SharedPreferences) {

    var gender by sharedPreferences.enumPreference(PREF_GENDER, Gender.ANY)
    var mode by sharedPreferences.enumPreference(PREF_MODE, Mode.STARTS)

    companion object {
        private const val PREF_GENDER = "pref_gender"
        private const val PREF_MODE = "pref_mode"
    }
}
