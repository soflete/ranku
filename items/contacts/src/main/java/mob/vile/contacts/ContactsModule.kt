package mob.vile.contacts

import android.app.Activity
import android.database.Cursor
import android.provider.ContactsContract
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
object ContactsModule {

    @Provides
    internal fun projection() = arrayOf(
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.LOOKUP_KEY,
            ContactsContract.Contacts.DISPLAY_NAME
    )

    @Provides
    internal fun cursorLoader(
            activity: Activity,
            projection: Array<String>
    ): Loader<Cursor> = CursorLoader(
            activity,
            ContactsContract.Contacts.CONTENT_URI,
            projection,
            null,
            null,
            "${ContactsContract.Contacts.DISPLAY_NAME} COLLATE LOCALIZED ASC"
    )
}
