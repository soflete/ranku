package mob.vile.contacts

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import com.androideas.core.data.Item
import com.androideas.core.data.Type
import dagger.Lazy
import dagger.hilt.android.AndroidEntryPoint
import mob.vile.contacts.databinding.ActivityAddContactBinding
import javax.inject.Inject

@AndroidEntryPoint
class ContactsFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {

    @Inject
    internal lateinit var loaderManager: LoaderManager

    @Inject
    internal lateinit var loader: Lazy<Loader<Cursor>>

    private lateinit var binding: ActivityAddContactBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        this.binding = ActivityAddContactBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.binding.list.setOnItemClickListener { adapterView, _, i, _ ->
            (adapterView.getItemAtPosition(i) as Cursor).run {
                val displayName = getString(getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val lookupKey = getString(getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY))

                Item(0, displayName, Type.CONTACT, lookupKey)
            }.let {
                activity?.apply {
                    val intent = Intent().putExtra(Item::class.simpleName, it)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
        }

        loaderManager.initLoader(0, Bundle.EMPTY, this)
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor?) {
        this.binding.list.adapter = SimpleCursorAdapter(context,
                android.R.layout.simple_list_item_1, data,
                arrayOf(ContactsContract.Contacts.DISPLAY_NAME),
                intArrayOf(android.R.id.text1), 0)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> = loader.get()

    override fun onLoaderReset(loader: Loader<Cursor>) {

    }
}
