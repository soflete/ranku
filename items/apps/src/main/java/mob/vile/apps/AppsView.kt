package mob.vile.apps

import com.androideas.core.data.Item

interface AppsView {
    fun listApps(apps: List<Item>)
}
