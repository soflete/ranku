package mob.vile.apps

import com.androideas.core.data.Item

data class AppsState(
        val apps: List<Item>? = null
)
