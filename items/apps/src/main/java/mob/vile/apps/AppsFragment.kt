package mob.vile.apps

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.ListFragment
import androidx.fragment.app.viewModels
import com.androideas.core.data.Item
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AppsFragment : ListFragment(), AppsView {

    private val viewModel: AppsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.liveState.observe(viewLifecycleOwner, AppsPresenter(this))
    }

    override fun listApps(apps: List<Item>) {
        listAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, apps)
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        with(l) {
            val item = getItemAtPosition(position) as Item
            activity?.apply {
                setResult(Activity.RESULT_OK, Intent().putExtra(Item::class.simpleName, item as Parcelable))
                finish()
            }
        }
    }
}
