package mob.vile.apps

import androidx.lifecycle.Observer

class AppsPresenter(private val view: AppsView) : Observer<AppsState> {
    override fun onChanged(value: AppsState) {
        value.apps?.also { view.listApps(it) }
    }
}
