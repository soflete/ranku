package mob.vile.apps

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.androideas.core.extension.reactIn
import com.androideas.core.extension.reduceIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AppsViewModel @Inject constructor(reactor: AppsReactor) : ViewModel() {

    private val events = MutableSharedFlow<AppsEvent>()
    private val state = MutableStateFlow(AppsState())

    val liveState = state.asLiveData()

    init {
        events.reactIn(viewModelScope, reactor::react)
        state.reduceIn(viewModelScope, events, ::reduce)
        viewModelScope.launch { events.emit(AppsEvent.LoadApps) }
    }
}
