package mob.vile.apps

import javax.inject.Inject

class AppsReactor @Inject constructor(private val getAppsUseCase: GetAppsUseCase) {

    suspend fun react(event: AppsEvent): AppsEvent? = when (event) {
        AppsEvent.LoadApps -> AppsEvent.AppsLoaded(getAppsUseCase.execute())
        else -> null
    }
}
