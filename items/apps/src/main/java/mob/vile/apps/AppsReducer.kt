package mob.vile.apps

fun reduce(state: AppsState, event: AppsEvent) = when (event) {
    is AppsEvent.LoadApps -> state
    is AppsEvent.AppsLoaded -> state.copy(apps = event.apps)
}

