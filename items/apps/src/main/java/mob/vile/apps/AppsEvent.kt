package mob.vile.apps

import com.androideas.core.data.Item

sealed class AppsEvent {
    object LoadApps : AppsEvent()
    data class AppsLoaded(val apps: List<Item>) : AppsEvent()
}
