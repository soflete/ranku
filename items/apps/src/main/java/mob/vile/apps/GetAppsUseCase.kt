package mob.vile.apps

import android.content.Intent
import android.content.pm.PackageManager
import com.androideas.core.data.Item
import com.androideas.core.data.Type
import javax.inject.Inject

class GetAppsUseCase @Inject constructor(private val packageManager: PackageManager) {

    fun execute(): List<Item> {
        val intent = Intent(Intent.ACTION_MAIN, null).addCategory(Intent.CATEGORY_LAUNCHER)
        return packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).map {
            Item(
                    0,
                    it.activityInfo.applicationInfo.loadLabel(packageManager).toString(),
                    Type.APP,
                    it.activityInfo.packageName
            )
        }
    }
}
