package vile.mob.labels

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import com.androideas.core.data.Item
import com.androideas.core.data.Type
import vile.mob.labels.databinding.FragmentLabelsBinding

class LabelsFragment : Fragment() {

    private lateinit var binding: FragmentLabelsBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        this.binding = FragmentLabelsBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.binding.inputEditText.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    save()
                    true
                }
                else -> false
            }
        }

        this.binding.saveButton.setOnClickListener { save() }
    }

    private fun save() {
        val input = this.binding.inputEditText.text.toString()
        when {
            input.isNotBlank() -> Item(0, input, Type.LABEL, input)
                    .let { item -> Intent().putExtra(Item::class.simpleName, item) }
                    .let { intent ->
                        activity?.apply {
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                    }
        }
    }
}
