package com.example.movies

import javax.inject.Inject

class MoviesReactor @Inject constructor(private val getMoviesUseCase: GetMoviesUseCase) {

    suspend fun react(event: MoviesEvent): MoviesEvent? = when (event) {
        is MoviesEvent.LoadMovies -> when {
            event.query.isNullOrBlank() -> null
            else -> getMoviesUseCase.execute(event.query)
                    .let { MoviesEvent.MoviesLoaded(it.results) }
        }
        else -> null
    }
}
