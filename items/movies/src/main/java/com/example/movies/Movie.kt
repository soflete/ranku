package com.example.movies

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
        var id: Int,
        val title: String,
        @Json(name = "backdrop_path") var backdropPath: String,
        @Json(name = "original_title") var originalTitle: String,
        @Json(name = "release_date") var releaseDate: String,
        @Json(name = "poster_path") var posterPath: String,
) : Parcelable
