package com.example.movies

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.androideas.core.data.Item
import com.androideas.core.data.Type
import com.androideas.core.extension.textChanges
import com.example.movies.databinding.FragmentMoviesBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MoviesFragment : Fragment(), MoviesView {

    private val viewModel: MoviesViewModel by viewModels()

    private lateinit var binding: FragmentMoviesBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        this.binding = FragmentMoviesBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.liveState.observe(viewLifecycleOwner, MoviesPresenter(this))

        with(binding) {
            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    binding.txtInput.textChanges()
                        .debounce(386)
                        .collect { viewModel.onLoadMovies(it) }
                }
            }

            results.setOnItemClickListener { v, _, position, _ ->
                v.getItemAtPosition(position)
                    .let { it as Movie }
                    .run { Item(0, title, Type.MOVIE, "$id") }
                    .let { item -> Intent().putExtra(Item::class.simpleName, item as Parcelable) }
                    .let { intent ->
                        activity?.apply {
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                    }
            }
        }
    }

    override fun updateMovies(movies: List<Movie>?) {
        movies?.let { ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, it) }
            ?.let { this.binding.results.adapter = it }
    }
}
