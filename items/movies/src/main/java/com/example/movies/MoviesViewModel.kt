package com.example.movies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.androideas.core.extension.reactIn
import com.androideas.core.extension.reduceIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(private val reactor: MoviesReactor) : ViewModel() {

    private val events = MutableSharedFlow<MoviesEvent>()
    private val state = MutableStateFlow(MoviesState())

    val liveState = state.asLiveData()

    init {
        events.reactIn(viewModelScope, reactor::react)
        state.reduceIn(viewModelScope, events, ::reduce)
    }

    fun onLoadMovies(query: CharSequence?) {
        viewModelScope.launch { events.emit(MoviesEvent.LoadMovies(query)) }
    }
}
