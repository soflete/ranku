package com.example.movies

import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(
        private val service: TMDbService
) {
    suspend fun execute(query: CharSequence): SearchMovieResponse = service.searchMovie(query.toString())
}
