package com.example.movies

import com.example.movies.Movie

interface MoviesView {
    fun updateMovies(movies: List<Movie>?)
}
