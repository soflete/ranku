package com.example.movies

import androidx.lifecycle.Observer

/**
 * Created by leandro on 17/03/16.
 */
class MoviesPresenter(private val view: MoviesView) : Observer<MoviesState> {
    override fun onChanged(value: MoviesState) {
        view.updateMovies(value.movies)
    }
}
