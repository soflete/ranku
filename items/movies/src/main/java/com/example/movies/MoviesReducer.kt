package com.example.movies

fun reduce(state: MoviesState, event: MoviesEvent) = when (event) {
    is MoviesEvent.LoadMovies -> state.copy(movies = when {
        event.query.isNullOrEmpty() -> emptyList()
        else -> state.movies
    })
    is MoviesEvent.MoviesLoaded -> state.copy(movies = event.movies)
}
