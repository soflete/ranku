package com.example.movies

import retrofit2.http.GET
import retrofit2.http.Query

interface TMDbService {

    @GET("search/movie?search_type=ngram&api_key=$API_KEY")
    suspend fun searchMovie(@Query("query") query: String): SearchMovieResponse

    companion object {
        const val API_KEY = "a13b036818326ca801367fd8ba420dde"
    }
}
