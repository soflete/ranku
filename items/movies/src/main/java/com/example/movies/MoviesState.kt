package com.example.movies

data class MoviesState(val movies: List<Movie>? = null)
