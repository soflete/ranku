package com.example.movies

sealed class MoviesEvent {
    data class LoadMovies(val query: CharSequence? = null) : MoviesEvent()
    data class MoviesLoaded(val movies: List<Movie>) : MoviesEvent()
}
