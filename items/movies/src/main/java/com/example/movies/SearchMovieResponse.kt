package com.example.movies

import com.example.movies.Movie

data class SearchMovieResponse(
        val page: Int,
        val totalPages: Int,
        val totalResults: Int,
        val results: List<Movie>
)
