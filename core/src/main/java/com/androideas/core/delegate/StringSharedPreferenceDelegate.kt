package com.androideas.core.delegate

import android.content.SharedPreferences
import kotlin.reflect.KProperty

class StringSharedPreferenceDelegate(
        private val sharedPreferences: SharedPreferences,
        private val key: String,
        private val default: String?
) {

    operator fun getValue(thisRef: Any?, property: KProperty<*>): String? {
        return sharedPreferences.getString(key, default)
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String?) {
        sharedPreferences.edit().putString(key, value).apply()
    }
}
