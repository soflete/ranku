package com.androideas.core.delegate

import android.content.SharedPreferences
import kotlin.reflect.KProperty

class IntSharedPreferenceDelegate(
        private val sharedPreferences: SharedPreferences,
        private val key: String,
        private val default: Int
) {

    operator fun getValue(thisRef: Any?, property: KProperty<*>): Int {
        return sharedPreferences.getInt(key, default)
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }
}
