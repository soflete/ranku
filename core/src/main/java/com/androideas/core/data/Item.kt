package com.androideas.core.data

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "items")
data class Item(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        @ColumnInfo(name = "title") val title: String,
        @ColumnInfo(name = "type") val type: Int,
        @ColumnInfo(name = "tag") val tag: String
) : Parcelable {
    override fun toString() = "$title ($id, $tag)"
}
