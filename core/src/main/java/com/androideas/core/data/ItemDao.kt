package com.androideas.core.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ItemDao {

    @Query("SELECT i.* FROM items i " +
            "INNER JOIN ranking_item ri ON i.id == ri.item_id " +
            "WHERE ri.ranking_id = :rankingId " +
            "ORDER BY ri.position")
    fun getAllByRankingIdLive(rankingId: Long): LiveData<List<Item>>

    @Query("SELECT i.* FROM items i " +
            "INNER JOIN ranking_item ri ON i.id == ri.item_id " +
            "WHERE ri.ranking_id = :rankingId " +
            "ORDER BY ri.position")
    suspend fun getAllByRankingId(rankingId: Long): List<Item>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: Item): Long

    @Delete
    suspend fun delete(item: Item)
}
