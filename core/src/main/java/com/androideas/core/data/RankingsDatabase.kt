package com.androideas.core.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [
    Ranking::class,
    Item::class,
    RankingItem::class
], version = 1)
abstract class RankingsDatabase : RoomDatabase() {
    abstract fun rankingDao(): RankingDao
    abstract fun itemDao(): ItemDao
    abstract fun rankingItemDao(): RankingItemDao
}
