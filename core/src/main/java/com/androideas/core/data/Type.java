package com.androideas.core.data;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({Type.LABEL, Type.MOVIE, Type.CONTACT, Type.APP, Type.NAME})
public @interface Type {
    int LABEL = 0;
    int MOVIE = 1;
    int CONTACT = 2;
    int APP = 3;
    int NAME = 4;
}
