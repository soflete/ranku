package com.androideas.core.data

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "rankings")
data class Ranking(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        @ColumnInfo(name = "title") var title: String? = null,
        @ColumnInfo(name = "type") @Type var type: Int = 0
) : Parcelable {
    override fun toString(): String {
        return "$title ($id, $type)"
    }
}
