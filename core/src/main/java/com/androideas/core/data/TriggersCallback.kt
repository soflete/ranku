package com.androideas.core.data

import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase

object TriggersCallback : RoomDatabase.Callback() {
    override fun onCreate(db: SupportSQLiteDatabase) {
        super.onCreate(db)
        db.execSQL("CREATE TRIGGER IF NOT EXISTS increment_positions " +
                "BEFORE INSERT ON ranking_item " +
                "BEGIN " +
                "UPDATE ranking_item SET position = position + 1 " +
                "WHERE ranking_id = NEW.ranking_id AND position >= NEW.position; " +
                "END")

        db.execSQL("CREATE TRIGGER IF NOT EXISTS decrement_positions " +
                "AFTER DELETE ON ranking_item " +
                "BEGIN " +
                "UPDATE ranking_item SET position = position - 1 " +
                "WHERE ranking_id = OLD.ranking_id AND position > OLD.position; " +
                "END")
    }
}
