package com.androideas.core.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy

@Dao
interface RankingItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(rankingItem: RankingItem)
}
