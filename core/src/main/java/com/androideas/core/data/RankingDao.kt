package com.androideas.core.data

import androidx.room.*

@Dao
interface RankingDao {

    @Query("SELECT * FROM rankings")
    suspend fun getAll(): List<Ranking>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(ranking: Ranking)

    @Delete
    suspend fun delete(ranking: Ranking)
}
