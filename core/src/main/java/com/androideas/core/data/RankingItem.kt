package com.androideas.core.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.Companion.CASCADE

@Entity(tableName = "ranking_item", primaryKeys = ["item_id", "ranking_id"], foreignKeys = [
    ForeignKey(
            entity = Ranking::class,
            parentColumns = ["id"],
            childColumns = ["ranking_id"],
            onDelete = CASCADE
    ),
    ForeignKey(
            entity = Item::class,
            parentColumns = ["id"],
            childColumns = ["item_id"],
            onDelete = CASCADE
    )
])
data class RankingItem(
        @ColumnInfo(name = "item_id", index = true) val itemId: Long,
        @ColumnInfo(name = "ranking_id", index = true) val rankingId: Long,
        @ColumnInfo(name = "position") val position: Int
)
