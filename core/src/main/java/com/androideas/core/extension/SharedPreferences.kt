package com.androideas.core.extension

import android.content.SharedPreferences
import com.androideas.core.delegate.PreferenceProperty
import kotlin.properties.ReadWriteProperty

inline fun <reified T : Enum<T>> SharedPreferences.enumPreference(
        key: String,
        defaultValue: T
): ReadWriteProperty<Any, T> = PreferenceProperty(
        sharedPreferences = this,
        key = key,
        defaultValue = defaultValue,
        getter = { k, d -> enumValueOf(getString(k, null) ?: d.name) },
        setter = { k, v -> putString(k, v.name) }
)

fun SharedPreferences.stringSetPreference(
        key: String,
        defaultValue: Set<String>?
): ReadWriteProperty<Any, Set<String>?> = PreferenceProperty(
        sharedPreferences = this,
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getStringSet,
        setter = SharedPreferences.Editor::putStringSet
)

fun SharedPreferences.stringPreference(
        key: String,
        defaultValue: String? = null
): ReadWriteProperty<Any, String?> = PreferenceProperty(
        sharedPreferences = this,
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getString,
        setter = SharedPreferences.Editor::putString
)

fun SharedPreferences.longPreference(
        key: String,
        defaultValue: Long = 0L
): ReadWriteProperty<Any, Long> = PreferenceProperty(
        sharedPreferences = this,
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getLong,
        setter = SharedPreferences.Editor::putLong
)

fun SharedPreferences.floatPreference(
        key: String,
        defaultValue: Float = 0f
): ReadWriteProperty<Any, Float> = PreferenceProperty(
        sharedPreferences = this,
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getFloat,
        setter = SharedPreferences.Editor::putFloat
)

fun SharedPreferences.intPreference(
        key: String,
        defaultValue: Int = 0
): ReadWriteProperty<Any, Int> = PreferenceProperty(
        sharedPreferences = this,
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getInt,
        setter = SharedPreferences.Editor::putInt
)

fun SharedPreferences.booleanPreference(
        key: String,
        defaultValue: Boolean = false
): ReadWriteProperty<Any, Boolean> = PreferenceProperty(
        sharedPreferences = this,
        key = key,
        defaultValue = defaultValue,
        getter = SharedPreferences::getBoolean,
        setter = SharedPreferences.Editor::putBoolean
)
