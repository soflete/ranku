package com.androideas.core.extension

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.launch

fun <E> MutableSharedFlow<E>.reactIn(scope: CoroutineScope, react: suspend (E) -> E?) {
    scope.launch { mapNotNull(react).collect { scope.launch { emit(it) } } }
}

fun <E, S> MutableSharedFlow<E>.reactIn(
        scope: CoroutineScope,
        state: StateFlow<S>,
        react: suspend (E, S) -> E?
) {
    scope.launch { mapNotNull { react(it, state.value) }.collect { launch { emit(it) } } }
}
