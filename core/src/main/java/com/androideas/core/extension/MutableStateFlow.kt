package com.androideas.core.extension

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

fun <S, E> MutableStateFlow<S>.reduceIn(
        scope: CoroutineScope,
        events: MutableSharedFlow<E>,
        reduce: suspend (S, E) -> S
) {
    scope.launch { events.map { reduce(value, it) }.collect(::emit) }
}
