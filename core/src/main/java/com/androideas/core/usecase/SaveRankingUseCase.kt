package com.androideas.core.usecase

import com.androideas.core.data.Ranking
import com.androideas.core.data.RankingsDatabase

class SaveRankingUseCase(private val database: RankingsDatabase) {
    suspend fun execute(ranking: Ranking) = database.rankingDao().insert(ranking)
}
