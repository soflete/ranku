package com.androideas.core.usecase

import com.androideas.core.data.Item
import com.androideas.core.data.RankingsDatabase

class DeleteItemUseCase(private val database: RankingsDatabase) {
    suspend fun execute(item: Item): Unit = database.itemDao().delete(item)
}
