package com.androideas.core.usecase

import com.androideas.core.data.Item
import com.androideas.core.data.Ranking
import com.androideas.core.data.RankingItem
import com.androideas.core.data.RankingsDatabase

class PostItemRankingUseCase(private val database: RankingsDatabase) {

    suspend fun execute(item: Item, ranking: Ranking, position: Int) {
        val id = database.itemDao().insert(item)
        database.rankingItemDao().insert(RankingItem(id, ranking.id, position))
    }
}
