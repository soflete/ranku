package com.androideas.core.usecase

import com.androideas.core.data.Ranking
import com.androideas.core.data.RankingsDatabase

class GetItemsUseCase constructor(private val database: RankingsDatabase) {
    fun live(ranking: Ranking) = database.itemDao().getAllByRankingIdLive(ranking.id)

    suspend fun execute(ranking: Ranking) = database.itemDao().getAllByRankingId(ranking.id)
}
