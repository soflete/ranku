package com.androideas.core.usecase

import com.androideas.core.data.Ranking
import com.androideas.core.data.RankingsDatabase

class DeleteRankingUseCase(private val database: RankingsDatabase) {
    suspend fun execute(ranking: Ranking) = database.rankingDao().delete(ranking)
}
