package com.androideas.core.usecase

import com.androideas.core.data.RankingsDatabase

class GetRankingsUseCase(private val database: RankingsDatabase) {
    suspend fun execute() = database.rankingDao().getAll()
}
