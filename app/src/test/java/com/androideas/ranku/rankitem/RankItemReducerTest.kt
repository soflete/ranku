package com.androideas.ranku.rankitem

import com.androideas.core.data.Item
import com.androideas.core.data.Type
import org.junit.Assert.*
import org.junit.Test

class RankItemReducerTest {

    @Test
    fun firstChooseNewFinished() {
        RankItemReducer.apply(RankItemState(FIRST, listOf(SECOND)), RankItemEvent.ChooseNew).apply {
            assertEquals(-1, endPosition)
            assertEquals(0, currentPosition)
            assertTrue(isFinished)
        }
    }

    @Test
    fun secondChooseComparedFinished() {
        RankItemReducer.apply(RankItemState(SECOND, listOf(FIRST)), RankItemEvent.ChooseCompared).apply {
            assertEquals(1, startPosition)
            assertEquals(1, currentPosition)
            assertTrue(isFinished)
        }
    }

    @Test
    fun secondChooseNew() {
        RankItemReducer.apply(RankItemState(SECOND, listOf(FIRST, THIRD)), RankItemEvent.ChooseNew).apply {
            assertEquals(0, endPosition)
            assertEquals(0, currentPosition)
            assertFalse(isFinished)
        }
    }

    @Test
    fun secondChooseCompared() {
        RankItemReducer.apply(RankItemState(SECOND, listOf(FIRST, THIRD)), RankItemEvent.ChooseCompared).apply {
            assertEquals(1, startPosition)
            assertEquals(1, currentPosition)
            assertFalse(isFinished)
        }
    }

    companion object {
        private val FIRST = Item(0, "first", Type.LABEL, "first")
        private val SECOND = Item(1, "second", Type.LABEL, "second")
        private val THIRD = Item(2, "third", Type.LABEL, "third")
    }
}
