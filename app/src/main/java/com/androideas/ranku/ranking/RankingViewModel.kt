package com.androideas.ranku.ranking

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.androideas.core.data.Item
import com.androideas.core.data.Ranking
import com.androideas.core.extension.reactIn
import com.androideas.core.extension.reduceIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RankingViewModel @Inject constructor(
        state: MutableStateFlow<RankingState>,
        reactor: RankingReactor
) : ViewModel() {

    private val events = MutableSharedFlow<RankingEvent>()

    val liveEvents = events.asLiveData()
    val liveState = state.asLiveData()

    init {
        events.reactIn(viewModelScope, state, reactor::react)
        state.reduceIn(viewModelScope, events, ::reduce)
        viewModelScope.launch { events.emit(RankingEvent.LoadItems) }
    }

    fun onItemRanked(item: Item, position: Int) {
        viewModelScope.launch { events.emit(RankingEvent.ItemRanked(item, position)) }
    }

    fun deleteItem(item: Item) {
        viewModelScope.launch { events.emit(RankingEvent.DeleteItem(item)) }
    }

    fun onItemAdded(item: Item) {
        viewModelScope.launch { events.emit(RankingEvent.ItemAdded(item)) }
    }

    fun addItem(ranking: Ranking) {
        viewModelScope.launch { events.emit(RankingEvent.AddItem(ranking)) }
    }
}
