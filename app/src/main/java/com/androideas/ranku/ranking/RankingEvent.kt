package com.androideas.ranku.ranking

import com.androideas.core.data.Item
import com.androideas.core.data.Ranking

sealed class RankingEvent {
    object LoadItems : RankingEvent()
    data class ItemsLoaded(val items: List<Item>) : RankingEvent()
    data class DeleteItem(val item: Item) : RankingEvent()
    data class ItemRanked(val item: Item, val position: Int) : RankingEvent()
    data class AddItem(val ranking: Ranking) : RankingEvent()
    data class ItemAdded(val item: Item) : RankingEvent()
    data class RankItem(val ranking: Ranking, val item: Item) : RankingEvent()
}
