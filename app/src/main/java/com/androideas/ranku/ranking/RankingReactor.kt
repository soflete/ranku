package com.androideas.ranku.ranking

import com.androideas.core.usecase.DeleteItemUseCase
import com.androideas.core.usecase.GetItemsUseCase
import com.androideas.core.usecase.PostItemRankingUseCase
import javax.inject.Inject

class RankingReactor @Inject constructor(
        private val getItemsUseCase: GetItemsUseCase,
        private val deleteItemUseCase: DeleteItemUseCase,
        private val postItemRankingUseCase: PostItemRankingUseCase,
) {

    suspend fun react(event: RankingEvent, state: RankingState): RankingEvent? = when (event) {
        is RankingEvent.ItemRanked -> with(event) {
            postItemRankingUseCase.execute(item, state.ranking, position)
            RankingEvent.LoadItems
        }
        is RankingEvent.ItemAdded -> RankingEvent.RankItem(state.ranking, event.item)
        is RankingEvent.DeleteItem -> deleteItemUseCase.execute(event.item)
                .let { RankingEvent.LoadItems }
        is RankingEvent.LoadItems -> getItemsUseCase.execute(state.ranking)
                .let { RankingEvent.ItemsLoaded(it) }
        else -> null
    }
}
