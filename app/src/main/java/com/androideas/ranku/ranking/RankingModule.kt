package com.androideas.ranku.ranking

import androidx.core.os.bundleOf
import androidx.lifecycle.SavedStateHandle
import com.androideas.core.data.Ranking
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.MutableStateFlow
import java.lang.IllegalArgumentException

@Module
@InstallIn(ViewModelComponent::class)
object RankingModule {

    private const val ARG_RANKING = "ranking"

    fun newRankingFragment(ranking: Ranking) = RankingFragment().apply {
        arguments = bundleOf(ARG_RANKING to ranking)
    }

    @Provides
    fun initialState(savedStateHandle: SavedStateHandle) = RankingState(
            savedStateHandle[ARG_RANKING] ?: throw IllegalArgumentException()
    )

    @Provides
    @ViewModelScoped
    fun stateFlow(initialState: RankingState) = MutableStateFlow(initialState)
}
