package com.androideas.ranku.ranking

fun reduce(state: RankingState, event: RankingEvent) = when (event) {
    is RankingEvent.ItemsLoaded -> state.copy(items = event.items)
    else -> state
}

