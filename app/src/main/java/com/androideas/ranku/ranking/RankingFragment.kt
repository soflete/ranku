package com.androideas.ranku.ranking

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.ListFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.androideas.core.data.Item
import com.androideas.ranku.R
import com.androideas.ranku.additem.AddItemActivity
import com.androideas.ranku.databinding.FragmentRankingBinding
import com.androideas.ranku.rankitem.RankItemActivity
import com.androideas.ranku.rankitem.RankItemFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RankingFragment : ListFragment() {

    private val viewModel: RankingViewModel by viewModels()

    private lateinit var binding: FragmentRankingBinding

    private val addItem = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        with(it) {
            when (resultCode) {
                Activity.RESULT_OK -> data?.apply {
                    val item = getParcelableExtra<Item>(Item::class.simpleName)!!
                    lifecycleScope.launchWhenResumed { viewModel.onItemAdded(item) }
                }
            }
        }
    }

    private val rankItem = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        with(it) {
            when (resultCode) {
                Activity.RESULT_OK -> data?.run {
                    val item = getParcelableExtra<Item>(RankItemFragment.EXTRA_ITEM)!!
                    val position = getIntExtra(RankItemFragment.EXTRA_POSITION, 0)
                    lifecycleScope.launchWhenResumed { viewModel.onItemRanked(item, position) }
                }
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        this.binding = FragmentRankingBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerForContextMenu(listView)

        viewModel.liveEvents.observe(viewLifecycleOwner) {
            when (it) {
                is RankingEvent.AddItem -> {
                    val intent = Intent(context, AddItemActivity::class.java)
                            .putExtra(AddItemActivity.EXTRA_TYPE, it.ranking.type)
                    addItem.launch(intent)
                }
                is RankingEvent.RankItem -> {
                    val intent = RankItemActivity.newIntent(requireContext(), it.ranking, it.item)
                    rankItem.launch(intent)
                }
                else -> {
                    // Nothing
                }
            }
        }

        viewModel.liveState.observe(viewLifecycleOwner) { (ranking, items) ->

            this.binding.fab.setOnClickListener { viewModel.addItem(ranking) }

            items?.let {
                listAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, it)
            }
        }
    }

    override fun onContextItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.remove -> {
            with(item.menuInfo as AdapterView.AdapterContextMenuInfo) {
                viewModel.deleteItem(listAdapter?.getItem(position) as Item)
            }
            true
        }
        else -> super.onContextItemSelected(item)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.apply {
            menuInflater.inflate(R.menu.context_menu, menu)
        }
    }
}
