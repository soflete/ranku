package com.androideas.ranku.ranking

import com.androideas.core.data.Item
import com.androideas.core.data.Ranking

data class RankingState(
        val ranking: Ranking,
        val items: List<Item>? = null
)
