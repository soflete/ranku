package com.androideas.ranku.ranking

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RankingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            val fragment = RankingModule.newRankingFragment(intent.getParcelableExtra(EXTRA_RANKING)!!)
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .commit()
        }
    }

    companion object {
        const val EXTRA_RANKING = "extra_ranking"
    }
}
