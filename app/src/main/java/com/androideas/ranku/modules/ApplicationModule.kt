package com.androideas.ranku.modules

import android.content.Context
import android.content.pm.PackageManager
import androidx.room.Room
import com.androideas.core.data.RankingsDatabase
import com.androideas.core.data.TriggersCallback
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Provides
    fun packageManager(@ApplicationContext context: Context): PackageManager = context.packageManager

    @Provides
    fun database(@ApplicationContext context: Context) = Room.databaseBuilder(context, RankingsDatabase::class.java, "rank-u")
            .addCallback(TriggersCallback)
            .build()
}
