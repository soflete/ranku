package com.androideas.ranku.modules

import com.androideas.core.data.RankingsDatabase
import com.androideas.core.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {

    @Provides
    fun deleteItemUseCase(database: RankingsDatabase) = DeleteItemUseCase(database)

    @Provides
    fun deleteRankingUseCase(database: RankingsDatabase) = DeleteRankingUseCase(database)

    @Provides
    fun getItemsUseCase(database: RankingsDatabase) = GetItemsUseCase(database)

    @Provides
    fun getRankingsUseCase(database: RankingsDatabase) = GetRankingsUseCase(database)

    @Provides
    fun postItemRankingUseCase(database: RankingsDatabase) = PostItemRankingUseCase(database)

    @Provides
    fun saveRankingUseCase(database: RankingsDatabase) = SaveRankingUseCase(database)
}
