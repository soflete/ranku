package com.androideas.ranku.rankings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.androideas.core.data.Ranking
import com.androideas.ranku.R
import com.androideas.ranku.databinding.FragmentRankingsBinding
import com.androideas.ranku.editranking.EditRankingActivity
import com.androideas.ranku.editranking.EditRankingFragment
import com.androideas.ranku.ranking.RankingActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RankingsFragment : androidx.fragment.app.ListFragment(), RankingsView {

    private val viewModel: RankingsViewModel by viewModels()

    private lateinit var binding: FragmentRankingsBinding
    private lateinit var editRanking: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        editRanking = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            with(it) {
                when (resultCode) {
                    Activity.RESULT_OK -> data?.ranking?.let(viewModel::saveRanking)
                }
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        this.binding = FragmentRankingsBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerForContextMenu(listView)

        with(this.binding) {
            fab.setOnClickListener {
                val intent = Intent(activity, EditRankingActivity::class.java)
                editRanking.launch(intent)
            }
        }

        viewModel.liveState.observe(viewLifecycleOwner, RankingsPresenter(this))
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.apply { menuInflater.inflate(R.menu.context_ranking, menu) }
    }

    override fun onContextItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.edit -> {
            val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
            val ranking = listAdapter?.getItem(info.position) as Ranking
            val intent = Intent(activity, EditRankingActivity::class.java)
                    .putExtra(EditRankingActivity.EXTRA_RANKING, ranking)
            editRanking.launch(intent)
            true
        }
        R.id.remove -> {
            val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
            val ranking = listAdapter?.getItem(info.position) as Ranking
            viewModel.removeRanking(ranking)
            true
        }
        else -> super.onContextItemSelected(item)
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        with(l) {
            val ranking = getItemAtPosition(position) as Ranking
            val intent = Intent(context, RankingActivity::class.java)
                    .putExtra(RankingActivity.EXTRA_RANKING, ranking)
            startActivity(intent)
        }
    }

    override fun listRankings(rankings: List<Ranking>) {
        context?.let {
            listAdapter = ArrayAdapter(it, android.R.layout.simple_list_item_1, rankings)
        }
    }

    private val Intent.ranking
        get() = getParcelableExtra<Ranking>(EditRankingFragment.EXTRA_RANKING)
}
