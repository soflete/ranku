package com.androideas.ranku.rankings

import com.androideas.core.data.Ranking

interface RankingsView {
    fun listRankings(rankings: List<Ranking>)
}
