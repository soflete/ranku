package com.androideas.ranku.rankings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.androideas.core.data.Ranking
import com.androideas.core.extension.reactIn
import com.androideas.core.extension.reduceIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RankingsViewModel @Inject constructor(reactor: RankingsModel.Reactor) : ViewModel() {

    private val events = MutableSharedFlow<RankingsModel.Event>()
    private val state = MutableStateFlow(RankingsModel.State())

    val liveState = state.asLiveData()

    init {
        events.reactIn(viewModelScope, reactor::react)
        state.reduceIn(viewModelScope, events, RankingsModel::reduce)
        viewModelScope.launch { events.emit(RankingsModel.Event.LoadRankings) }
    }

    fun saveRanking(ranking: Ranking) {
        viewModelScope.launch { events.emit(RankingsModel.Event.SaveRanking(ranking)) }
    }

    fun removeRanking(ranking: Ranking) {
        viewModelScope.launch { events.emit(RankingsModel.Event.RemoveRanking(ranking)) }
    }
}
