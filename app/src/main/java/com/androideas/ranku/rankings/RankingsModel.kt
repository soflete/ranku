package com.androideas.ranku.rankings

import com.androideas.core.data.Ranking
import com.androideas.core.usecase.DeleteRankingUseCase
import com.androideas.core.usecase.GetRankingsUseCase
import com.androideas.core.usecase.SaveRankingUseCase

class RankingsModel {

    data class State(
            val isLoading: Boolean = false,
            val rankings: List<Ranking>? = null
    ) {
        fun reduce(event: Event) = when (event) {
            Event.LoadRankings -> copy(isLoading = true)
            is Event.RankingsLoaded -> copy(isLoading = false, rankings = event.rankings)
            is Event.RemoveRanking -> copy(rankings = rankings?.filter { it != event.ranking })
            is Event.SaveRanking -> this
        }
    }

    sealed class Event {
        object LoadRankings : Event()
        data class RankingsLoaded(val rankings: List<Ranking>) : Event()
        data class RemoveRanking(val ranking: Ranking) : Event()
        data class SaveRanking(val ranking: Ranking) : Event()
    }

    class Reactor(
            private val getRankingsUseCase: GetRankingsUseCase,
            private val deleteRankingUseCase: DeleteRankingUseCase,
            private val saveRankingUseCase: SaveRankingUseCase
    ) {

        suspend fun react(event: Event): Event? = when (event) {
            is Event.LoadRankings -> with(event) {
                val result = getRankingsUseCase.execute()
                Event.RankingsLoaded(result)
            }
            is Event.RemoveRanking -> with(event) {
                deleteRankingUseCase.execute(ranking)
                Event.LoadRankings
            }
            is Event.SaveRanking -> with(event) {
                saveRankingUseCase.execute(ranking)
                Event.LoadRankings
            }
            else -> null
        }
    }

    companion object {
        fun reduce(state: State, event: Event) = state.reduce(event)
    }
}
