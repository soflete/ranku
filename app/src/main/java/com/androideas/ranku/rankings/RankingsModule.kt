package com.androideas.ranku.rankings

import com.androideas.core.usecase.DeleteRankingUseCase
import com.androideas.core.usecase.GetRankingsUseCase
import com.androideas.core.usecase.SaveRankingUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class RankingsModule {

    @Provides
    fun provideReactor(
            getRankingsUseCase: GetRankingsUseCase,
            deleteRankingUseCase: DeleteRankingUseCase,
            saveRankingUseCase: SaveRankingUseCase
    ) = RankingsModel.Reactor(
            getRankingsUseCase,
            deleteRankingUseCase,
            saveRankingUseCase
    )
}
