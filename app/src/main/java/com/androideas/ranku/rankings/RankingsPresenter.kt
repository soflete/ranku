package com.androideas.ranku.rankings

import androidx.lifecycle.Observer

class RankingsPresenter(
    private val view: RankingsView
) : Observer<RankingsModel.State> {
    override fun onChanged(value: RankingsModel.State) {
        value.rankings?.also { view.listRankings(it) }
    }
}
