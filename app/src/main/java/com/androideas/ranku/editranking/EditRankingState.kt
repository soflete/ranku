package com.androideas.ranku.editranking

import com.androideas.core.data.Ranking

data class EditRankingState(
        val ranking: Ranking = Ranking(),
        val isEdit: Boolean = false
) {
    val isValid = !ranking.title.isNullOrBlank()
}
