package com.androideas.ranku.editranking

import com.androideas.core.data.Ranking

interface EditRankingView {
    fun showRanking(title: String?, type: Int, typeEnabled: Boolean)
    fun showEmptyTitleError()
    fun configureSaveRankingButton(ranking: Ranking)
}
