package com.androideas.ranku.editranking

import com.androideas.core.data.Ranking

sealed class EditRankingEvent {
    internal data class LoadRanking(val ranking: Ranking) : EditRankingEvent()
    internal data class SaveRanking(val ranking: Ranking) : EditRankingEvent()
}
