package com.androideas.ranku.editranking

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EditRankingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, EditRankingFragment
                            .newInstance(intent.getParcelableExtra(EXTRA_RANKING)))
                    .commit()
        }
    }

    companion object {
        const val EXTRA_RANKING = "extra_ranking"
    }
}
