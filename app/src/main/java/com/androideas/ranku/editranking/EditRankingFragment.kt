package com.androideas.ranku.editranking

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.androideas.core.data.Ranking
import com.androideas.core.data.Type
import com.androideas.ranku.R
import com.androideas.ranku.databinding.FragmentEditRankingBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EditRankingFragment : Fragment(), EditRankingView {

    private val viewModel: EditRankingViewModel by viewModels()
    private lateinit var binding: FragmentEditRankingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        when (savedInstanceState) {
            null -> requireArguments().ranking?.let { viewModel.loadRanking(it) }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        this.binding = FragmentEditRankingBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(this.binding) {
            txtInput.requestFocus()
            spnType.adapter = ArrayAdapter.createFromResource(
                    requireContext(), R.array.types, android.R.layout.simple_spinner_item
            ).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
        }

        viewModel.liveEvents.observe(viewLifecycleOwner) {
            if (it is EditRankingEvent.SaveRanking) {
                activity?.apply {
                    Intent().putExtra(EXTRA_RANKING, it.ranking).let { intent ->
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                }
            }
        }

        viewModel.liveState.observe(viewLifecycleOwner, EditRankingPresenter(this))
    }

    override fun showRanking(title: String?, type: Int, typeEnabled: Boolean) {
        with(binding) {
            txtInput.setText(title)
            spnType.setSelection(type)
            spnType.isEnabled = typeEnabled
        }
    }

    override fun configureSaveRankingButton(ranking: Ranking) {

        with(binding) {

            txtInput.setOnEditorActionListener { _, actionId, _ ->
                return@setOnEditorActionListener when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> {
                        save(ranking)
                        true
                    }
                    else -> false
                }
            }

            btnGo.setOnClickListener { save(ranking) }
        }
    }

    private fun FragmentEditRankingBinding.save(ranking: Ranking) {
        val title = txtInput.text.toString()
        val type = toSelectedType(spnType.selectedItemPosition)
        viewModel.saveRanking(ranking.copy(title = title, type = type))
    }

    override fun showEmptyTitleError() {
        this.binding.txtInput.error = "Item name cannot be empty."
    }

    private val Bundle.ranking
        get() = getParcelable<Ranking>(ARG_RANKING)

    companion object {

        private const val ARG_RANKING = "ranking";
        const val EXTRA_RANKING = "extra_ranking"

        @Type
        private fun toSelectedType(selectedItemPosition: Int) = when (selectedItemPosition) {
            1 -> Type.MOVIE
            2 -> Type.CONTACT
            3 -> Type.APP
            4 -> Type.NAME
            else -> Type.LABEL
        }

        fun newInstance(ranking: Ranking?) = EditRankingFragment().apply {
            arguments = bundleOf(
                    ARG_RANKING to ranking
            )
        }
    }
}
