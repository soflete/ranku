package com.androideas.ranku.editranking

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.androideas.core.data.Ranking
import com.androideas.core.extension.reduceIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EditRankingViewModel @Inject constructor() : ViewModel() {

    private val events = MutableSharedFlow<EditRankingEvent>()
    private val state = MutableStateFlow(EditRankingState())

    val liveEvents = events.asLiveData()
    val liveState = state.asLiveData()

    init {
        state.reduceIn(viewModelScope, events, ::reduce)
    }

    fun loadRanking(ranking: Ranking) {
        viewModelScope.launch { events.emit(EditRankingEvent.LoadRanking(ranking)) }
    }

    fun saveRanking(ranking: Ranking) {
        viewModelScope.launch { events.emit(EditRankingEvent.SaveRanking(ranking)) }
    }
}
