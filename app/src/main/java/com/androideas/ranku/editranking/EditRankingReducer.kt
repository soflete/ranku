package com.androideas.ranku.editranking

fun reduce(state: EditRankingState, event: EditRankingEvent) = when (event) {
    is EditRankingEvent.LoadRanking -> state.copy(ranking = event.ranking)
    is EditRankingEvent.SaveRanking -> state.copy(ranking = event.ranking)
}
