package com.androideas.ranku.editranking

import androidx.lifecycle.Observer

class EditRankingPresenter(
    private val view: EditRankingView
) : Observer<EditRankingState> {
    override fun onChanged(value: EditRankingState) {
        value.apply {
            ranking.apply {
                view.showRanking(title = title, type = type, typeEnabled = !isEdit)
                view.configureSaveRankingButton(ranking)
            }
            when {
                !isValid -> view.showEmptyTitleError()
            }
        }
    }
}
