package com.androideas.ranku.rankitem

fun reduce(state: RankItemState, event: RankItemEvent) = when (event) {
    is RankItemEvent.LoadItems -> state
    is RankItemEvent.ItemsLoaded -> state.copy(
            items = event.items,
            endPosition = event.items.lastIndex
    )
    RankItemEvent.ChooseNew -> with(state) {
        copy(endPosition = currentPosition - 1)
    }
    RankItemEvent.ChooseCompared -> with(state) {
        copy(startPosition = currentPosition + 1)
    }
}
