package com.androideas.ranku.rankitem

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.androideas.core.data.Item
import com.androideas.core.data.Ranking
import com.androideas.ranku.databinding.FragmentRankItemBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RankItemFragment : Fragment(), RankItemView {

    private val viewModel: RankItemViewModel by viewModels()

    private lateinit var binding: FragmentRankItemBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        this.binding = FragmentRankItemBinding.inflate(inflater, container, false)
        return this.binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            newItem.setOnClickListener { viewModel.onChooseNew() }
            comparedItem.setOnClickListener { viewModel.onChooseCompared() }
        }

        viewModel.liveState.observe(viewLifecycleOwner, RankItemPresenter(this))
    }

    override fun compare(item: Item) {
        this.binding.comparedItem.text = item.title
    }

    override fun showNewItem(item: Item) {
        this.binding.newItem.text = item.title
    }

    override fun deliverResult(item: Item, position: Int) {
        activity?.apply {
            setResult(Activity.RESULT_OK, Intent()
                    .putExtra(EXTRA_ITEM, item)
                    .putExtra(EXTRA_POSITION, position))
            finish()
        }
    }

    companion object {
        const val EXTRA_ITEM = "extra_item"
        const val EXTRA_POSITION = "extra_position"
    }
}
