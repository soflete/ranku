package com.androideas.ranku.rankitem

import com.androideas.core.data.Item

sealed class RankItemEvent {
    object LoadItems : RankItemEvent()
    data class ItemsLoaded(val items: List<Item>) : RankItemEvent()
    object ChooseNew : RankItemEvent()
    object ChooseCompared : RankItemEvent()
}
