package com.androideas.ranku.rankitem

import com.androideas.core.data.Item

interface RankItemView {
    fun compare(item: Item)

    fun showNewItem(item: Item)

    fun deliverResult(item: Item, position: Int)
}
