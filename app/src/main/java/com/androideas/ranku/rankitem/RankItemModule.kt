package com.androideas.ranku.rankitem

import androidx.core.os.bundleOf
import androidx.lifecycle.SavedStateHandle
import com.androideas.core.data.Item
import com.androideas.core.data.Ranking
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.MutableStateFlow

@Module
@InstallIn(ViewModelComponent::class)
object RankItemModule {

    private const val ARG_RANKING = "ranking"
    private const val ARG_ITEM = "item"

    fun newRankItemFragment(ranking: Ranking, item: Item) = RankItemFragment().apply {
        arguments = bundleOf(ARG_RANKING to ranking, ARG_ITEM to item)
    }

    @Provides
    fun initialState(savedStateHandle: SavedStateHandle) = RankItemState(
            savedStateHandle[ARG_RANKING] ?: throw IllegalArgumentException(),
            savedStateHandle[ARG_ITEM] ?: throw IllegalArgumentException()
    )

    @Provides
    @ViewModelScoped
    fun stateFlow(initialState: RankItemState) = MutableStateFlow(initialState)
}
