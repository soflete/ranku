package com.androideas.ranku.rankitem

import com.androideas.core.usecase.GetItemsUseCase
import javax.inject.Inject

class RankItemReactor @Inject constructor(private val getItemsUseCase: GetItemsUseCase) {

    suspend fun react(event: RankItemEvent, state: RankItemState): RankItemEvent? = when (event) {
        is RankItemEvent.LoadItems -> getItemsUseCase.execute(state.ranking)
                .let { RankItemEvent.ItemsLoaded(it) }
        else -> null
    }
}
