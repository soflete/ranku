package com.androideas.ranku.rankitem

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.androideas.core.extension.reactIn
import com.androideas.core.extension.reduceIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RankItemViewModel @Inject constructor(
        state: MutableStateFlow<RankItemState>,
        reactor: RankItemReactor
) : ViewModel() {

    private val events = MutableSharedFlow<RankItemEvent>()
    val liveState = state.asLiveData()

    init {
        events.reactIn(viewModelScope, state, reactor::react)
        state.reduceIn(viewModelScope, events, ::reduce)
        viewModelScope.launch { events.emit(RankItemEvent.LoadItems) }
    }

    fun onChooseNew() {
        viewModelScope.launch { events.emit(RankItemEvent.ChooseNew) }
    }

    fun onChooseCompared() {
        viewModelScope.launch { events.emit(RankItemEvent.ChooseCompared) }
    }
}
