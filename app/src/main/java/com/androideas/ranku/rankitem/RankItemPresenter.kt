package com.androideas.ranku.rankitem

import androidx.lifecycle.Observer

class RankItemPresenter(
    private val view: RankItemView
) : Observer<RankItemState> {

    override fun onChanged(t: RankItemState) {
        t.run {
            when {
                items != null -> when {
                    isFinished -> view.deliverResult(item, currentPosition)
                    else -> {
                        view.showNewItem(item)
                        view.compare(items[currentPosition])
                    }
                }
            }
        }
    }
}
