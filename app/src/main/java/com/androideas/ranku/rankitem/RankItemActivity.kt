package com.androideas.ranku.rankitem

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.androideas.core.data.Item
import com.androideas.core.data.Ranking
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RankItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            val fragment = RankItemModule.newRankItemFragment(intent.ranking, intent.item)
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .commit()
        }
    }

    private val Intent.ranking
        get() = getParcelableExtra<Ranking>(EXTRA_RANKING)!!

    private val Intent.item
        get() = getParcelableExtra<Item>(EXTRA_ITEM)!!

    companion object {
        const val EXTRA_ITEM = "extra_item"
        const val EXTRA_RANKING = "extra_ranking"

        fun newIntent(
                context: Context,
                ranking: Ranking,
                item: Item
        ) = Intent(context, RankItemActivity::class.java)
                .putExtra(EXTRA_RANKING, ranking)
                .putExtra(EXTRA_ITEM, item)
    }
}
