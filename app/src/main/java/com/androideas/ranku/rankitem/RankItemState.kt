package com.androideas.ranku.rankitem

import com.androideas.core.data.Item
import com.androideas.core.data.Ranking

data class RankItemState(
        val ranking: Ranking,
        val item: Item,
        val items: List<Item>? = null,
        val startPosition: Int = 0,
        val endPosition: Int = -1
) {
    val isFinished
        get() = endPosition < startPosition

    val currentPosition
        get() = when {
            isFinished -> startPosition
            else -> startPosition + (endPosition - startPosition) / 2
        }
}
