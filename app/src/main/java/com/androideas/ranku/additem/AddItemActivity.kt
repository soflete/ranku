package com.androideas.ranku.additem

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.loader.app.LoaderManager
import com.androideas.core.data.Type
import com.androideas.ranku.R
import com.example.movies.MoviesFragment
import com.example.names.NamesFragment
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.components.ActivityComponent
import mob.vile.apps.AppsFragment
import mob.vile.contacts.ContactsFragment
import vile.mob.labels.LabelsFragment

@AndroidEntryPoint
class AddItemActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            val fragment = when (intent.getIntExtra(EXTRA_TYPE, Type.LABEL)) {
                Type.MOVIE -> {
                    setTitle(R.string.add_movie)
                    MoviesFragment()
                }

                Type.CONTACT -> {
                    setTitle(R.string.add_contact)
                    ContactsFragment()
                }

                Type.APP -> {
                    setTitle(R.string.add_app)
                    AppsFragment()
                }

                Type.NAME -> {
                    setTitle(R.string.add_name)
                    NamesFragment()
                }

                else -> {
                    setTitle(R.string.add_item)
                    LabelsFragment()
                }
            }

            supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, fragment)
                .commit()
        }
    }

    @InstallIn(ActivityComponent::class)
    @dagger.Module
    object Module {

        @Provides
        fun loaderManager(activity: FragmentActivity): LoaderManager = activity.supportLoaderManager
    }

    companion object {
        const val EXTRA_TYPE = "extra_type"
    }
}
