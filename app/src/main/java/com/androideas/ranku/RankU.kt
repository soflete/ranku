package com.androideas.ranku

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RankU : Application()
